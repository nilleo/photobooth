#!/usr/bin/env python
import RPi.GPIO as GPIO
import time
from threading import Thread
import picamera
import PIL
from PIL import Image
import cups
import os
import sys
import pygame
import random
import shelve

slideshowRunning = True
basewidth = 177 ## Used for merging the photos onto one
imgPath = './images'

## Push button for starting the photo sequence
GPIO.setmode(GPIO.BCM)
GPIO.setup(18, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(17, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(27, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(22, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.add_event_detect(17,GPIO.FALLING,bouncetime=500)
GPIO.add_event_detect(27,GPIO.FALLING,bouncetime=500)
GPIO.add_event_detect(22,GPIO.FALLING,bouncetime=500)

pygame.mixer.pre_init(44100, -16, 1, 1024*3) ## PreInit Music, plays faster

## Display surface
pygame.init()

pygame.mouse.set_visible(0)

w = pygame.display.Info().current_w
h = pygame.display.Info().current_h

screenSize = (w, h)

screen = pygame.display.set_mode(screenSize, pygame.FULLSCREEN) ## Full screen the display with no window

## Used for overlay countdown
background = pygame.Surface(screen.get_size()) ## Create the background object
background = background.convert() ## Convert it to a background
screenMessage = ""


def PhotoProcess():
        global screenMessage
        pygame.mixer.music.load('/home/pi/photobooth/beep.wav')
	screenMessage = "5"
	UpdateDisplay()
        pygame.mixer.music.play()
	time.sleep(1)
	screenMessage = "4"
	UpdateDisplay()
        pygame.mixer.music.play()
	time.sleep(1)
	screenMessage = "3"
	UpdateDisplay()
        pygame.mixer.music.play()
	time.sleep(1)
	screenMessage = "2"
	UpdateDisplay()
        pygame.mixer.music.play()
	time.sleep(1)
	screenMessage = "1"
	UpdateDisplay()
        pygame.mixer.music.play()
	time.sleep(1)
	screenMessage = "SMILE"
	UpdateDisplay()
	time.sleep(1)

## Overlay Countdown Update
def UpdateDisplay():
	background.fill(pygame.Color("black")) ## Black background
        font = pygame.font.Font(None, 800)
        text = font.render(screenMessage, 1, (255,255,255))
        textpos = text.get_rect()
        textpos.centerx = background.get_rect().centerx
        textpos.centery = background.get_rect().centery
        background.blit(text, textpos)
        screen.blit(background, (0,0))
        pygame.display.flip()


## Used for loading a random photo for the slideshow
def random_file(dir):
	files = [os.path.join(path, filename)
		for path, dirs, files in os.walk(dir)
		for filename in files]

	return random.choice(files)


def displayImage(file):
	screen.fill((0,0,0))

	img = pygame.image.load(file) 
	img = pygame.transform.scale(img,(w,h)) ## Make the image full screen
	screen.blit(img,(0,0))
	pygame.display.flip() ## update the display


## Display a random image
def slideshow():
	# while True:

    if slideshowRunning == True:

        # checkEvents()

        randomFile = random_file(imgPath + '/combined')
        # randomFile = random_file('./images/')
        
        displayImage(randomFile)

        # time.sleep(2)
 

## Handle events like keypress
def checkEvents():
	for event in pygame.event.get():
		## Shutdown the application if quit event or escape key is pressed
		if event.type == pygame.QUIT or ( event.type is pygame.KEYDOWN and event.key == pygame.K_ESCAPE ):
			slideshowRunning = False
			pygame.quit()
			sys.exit()

		if event.type is pygame.KEYDOWN and event.key == pygame.K_f: ## Switch the display mode between full screen and windowed
			if screen.get_flags() & pygame.FULLSCREEN:
				pygame.display.set_mode(screenSize)
			else:
				pygame.display.set_mode(screenSize,pygame.FULLSCREEN)

## Get useremail
def get_email():
    userDone = -1
    emailPrompt = 'Enter email: '
    userEmail = ''
    # Get user input
    while userDone == -1:
        for event in pygame.event.get():
            if event.type is pygame.KEYDOWN and event.key == pygame.K_RETURN: 
                userDone = 1
            elif event.type is pygame.KEYDOWN: 
                # Listen for valid key press
                if event.key >= ord('!') and event.key <= ord('9') or \
                    event.key >= ord('?') and event.key <= ord('Z') or \
                    event.key >= ord('a') and event.key <= ord('~') or \
                    event.key == ord('^'):

                    # Shift modifier pressed
                    if pygame.key.get_mods() & pygame.KMOD_SHIFT:
                        # Capital letters, some people think this matters...
                        if event.key >= ord('a') and event.key <= ord('z'):
                            userEmail += str(unichr(event.key-32))
                        # @ symbol
                        elif event.key == ord('2'):
                            userEmail += '@'
                    else:
                        userEmail += str(unichr(event.key))
                elif event.key == pygame.K_BACKSPACE:
                    userEmail = userEmail[:-1]
            displayStatus(emailPrompt + userEmail)
    return userEmail

## On screen text message
def displayStatus(status):
	screen.fill((0,0,0))
	
	font = pygame.font.SysFont("monospace",72)
	text = font.render(status,True,(255,255,255))

	## Display in the center of the screen
	textrect = text.get_rect()
	textrect.centerx = screen.get_rect().centerx
	textrect.centery = screen.get_rect().centery

	screen.blit(text,textrect)
	
	pygame.display.flip() # update the display
	

## Merge all photos onto one ready for printing
##def combineImages():
##	displayStatus('Please wait. Processing Images')
##			
##	## Do the merging
##	blankImage = Image.open('blank.jpg')
##
##	image1 = Image.open(imgPath + '/image1.jpg')		
##	image1 = image1.resize((900,600),PIL.Image.ANTIALIAS)
##	blankImage.paste(image1, (0,0))
##
##	image2 = Image.open(imgPath + '/image2.jpg')		
##	image2 = image2.resize((900,600),PIL.Image.ANTIALIAS)
##	blankImage.paste(image2, (0,600))
##
##	image3 = Image.open(imgPath + '/image3.jpg')		
##	image3 = image3.resize((900,600),PIL.Image.ANTIALIAS)
##	blankImage.paste(image3, (900,0))
##
##	image4 = Image.open(imgPath + '/image4.jpg')		
##	image4 = image4.resize((900,600),PIL.Image.ANTIALIAS)
##	blankImage.paste(image4, (900,600))
##
##	blankImage.save(imgPath + '/combined.jpg', 'JPEG', quality=100)

def combineImages():
        displayStatus('Please wait. Processing Images...')
        blankImage = Image.open('blank.jpg')
        image1 = Image.open(imgPath + '/image1.jpg')		
        image1 = image1.resize((801,501),PIL.Image.ANTIALIAS)
        blankImage.paste(image1, (170,80))
        image2 = Image.open(imgPath + '/image2.jpg')		
        image2 = image2.resize((801,501),PIL.Image.ANTIALIAS)
        blankImage.paste(image2, (170,625))
        image3 = Image.open(imgPath + '/image3.jpg')		
        image3 = image3.resize((801,501),PIL.Image.ANTIALIAS)
        blankImage.paste(image3, (980,80))
        image4 = Image.open(imgPath + '/image4.jpg')		
        image4 = image4.resize((801,501),PIL.Image.ANTIALIAS)
        blankImage.paste(image4, (980,625))
        image5 = Image.open('finallogobar.png')
        blankImage.paste(image5, (0,0))
        blankImage.save(imgPath + '/combined.jpg', 'JPEG', quality=100)   

## Print the photo
def printPhoto():
	displayStatus('Printing...')

	conn = cups.Connection()
	printers = conn.getPrinters()
	printer_name = printers.keys()[0]
	conn.printFile(printer_name, imgPath + '/combined.jpg',"TITLE",{})

	# time.sleep(2)

## Thread for the slideshow
# t = Thread(target=slideshow)
# t.start()

def save_pages(pageCount):
    data = shelve.open('data')
    data['pages'] = pageCount
    data.close()

def load_pages():
    data = shelve.open('data')
    if len(sys.argv) == 2:
        data['pages'] = int(sys.argv[1])
    pagesLeft = data['pages']
    data.close()
    return pagesLeft


with picamera.PiCamera() as camera:
    pagesLeft = load_pages()
    print(str(pagesLeft))

    # initialize clock
    startTime = time.clock()
    # update clock counter to be 10 seconds past start marker
    # so that the slideshow starts right away
    newTime = startTime + 10
    while True:

        # checkEvents() ## Needed to check for keypresses and close signals

        # Check if 2 seconds have elapsed
        if (newTime - startTime) > 2:
            slideshow()
            startTime = time.clock()
            newTime = time.clock()
        # 2 sec have not elapsed, update clock
        else:
            newTime = time.clock()

        ## Putton press to start the photo sequence
        photoButton = GPIO.input(18)
        if photoButton == False:

            ## Stop the slideshow
            slideshowRunning = False
            # time.sleep(1)

            ## Loop through the 4 photo taking sequences
            for pNum in range (1,5):
                ## Initialise the camera object
                                ## Transparency allows pigame to shine through
                                camera.preview_alpha = 120
                                #camera.vflip = False
                                camera.hflip = False
                                #camera.rotation = 90
                                camera.brightness = 45
                                camera.exposure_compensation = 6
                                camera.contrast = 8
                                # camera.resolution = (1920,1080)
                                ## Start the preview
                                camera.start_preview()
                                camera.annotate_text = 'Photo ' + str(pNum) + ' of 4'
                                PhotoProcess()
                                camera.annotate_text = ''
                                camera.capture( imgPath + '/image' + str(pNum) + '.jpg')

            ## Stop the camera preview so we can return to the pygame surface
            camera.stop_preview()

            combineImages()

            displayImage( imgPath + '/combined.jpg' ) ## Display a preview of the combined image

            userSelection = -1
            while userSelection != 1:
                if GPIO.event_detected(17):
                    print ("Done")
                    userSelection = 1
                    break
                if GPIO.event_detected(27):
                    print ("Email")
                    userSelection = 2
                    userEmail = get_email()
                    print(userEmail)
                    continue
                if GPIO.event_detected(22):
                    print ("Print")
                    userSelection = 3
                    if (pagesLeft):
                        pagesLeft -= 1
                        save_pages(pagesLeft)
                        printPhoto()
                        time.sleep(20)
                        continue
                    else:
                        displayStatus('Out of Paper!')
                        time.sleep(2)
                        continue
                if userSelection > 1:
                    userSelection = -1
                    displayImage( imgPath + '/combined.jpg' ) 

            timestamp = str(int(time.time()))
            os.rename(imgPath + '/combined.jpg', imgPath + '/combined/' + timestamp + '.jpg')



            ## Move the temp files to a new dir based on the current timestamp so they can be retrieved later
            tmpdir = imgPath + '/' + timestamp
            if not os.path.exists(tmpdir):
                os.mkdir(tmpdir)
            os.rename(imgPath + '/image1.jpg', tmpdir + '/image1.jpg')
            os.rename(imgPath + '/image2.jpg', tmpdir + '/image2.jpg')
            os.rename(imgPath + '/image3.jpg', tmpdir + '/image3.jpg')
            os.rename(imgPath + '/image4.jpg', tmpdir + '/image4.jpg')

            ## Restart the slideshow
            slideshowRunning = True
