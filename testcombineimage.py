import PIL
from PIL import Image

## Do the merging
imgPath = '/home/pi/photobooth/images/test'
blankImage = Image.open('blank.jpg')
image1 = Image.open(imgPath + '/image1.jpg')		
image1 = image1.resize((801,501),PIL.Image.ANTIALIAS)
blankImage.paste(image1, (170,80))
image2 = Image.open(imgPath + '/image2.jpg')		
image2 = image2.resize((801,501),PIL.Image.ANTIALIAS)
blankImage.paste(image2, (170,625))
image3 = Image.open(imgPath + '/image3.jpg')		
image3 = image3.resize((801,501),PIL.Image.ANTIALIAS)
blankImage.paste(image3, (980,80))
image4 = Image.open(imgPath + '/image4.jpg')		
image4 = image4.resize((801,501),PIL.Image.ANTIALIAS)
blankImage.paste(image4, (980,625))
image5 = Image.open('finallogobar.png')
blankImage.paste(image5, (0,0))
blankImage.save(imgPath + '/combined.jpg', 'JPEG', quality=100)
